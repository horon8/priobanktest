﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLibrary
{
    public class DatabaseContext : DbContext
    {
        private const string NameConnectionString = "DailyInfoDB";

        public DatabaseContext() : base(NameConnectionString) { }

        public DbSet<CurrencyRateOnDate> CurrencyRatesOnDate { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CurrencyRateOnDate>()
                .Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<CurrencyRateOnDate>()
                .HasIndex(p => p.Date);

            base.OnModelCreating(modelBuilder);
        }
    }
}
