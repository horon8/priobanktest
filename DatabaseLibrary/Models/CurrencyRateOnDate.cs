﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLibrary
{
    public class CurrencyRateOnDate
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Курс на дату
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Название валюты
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номинал
        /// </summary>
        public decimal Denomination { get; set; }

        /// <summary>
        /// Курс
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        ///  ISO Цифровой код валюты
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// ISO Символьный код валюты
        /// </summary>
        public string ChCode { get; set; }
    }
}
