﻿using Quartz;
using Quartz.Impl;
using System;
using System.ServiceProcess;

namespace СentralBankWinService
{
    public partial class CBService : ServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IScheduler scheduler;

        public CBService()
        {
            InitializeComponent();
        }

        private void RunScheduler()
        {
            var factory = new StdSchedulerFactory();

            scheduler = factory.GetScheduler(); //Вся настройка scheduler находится в quartzJobsAndTriggers.xml
            scheduler.Start();

            var job = JobBuilder.Create<SyncCurrencyRatesJob>()
                .WithIdentity(Constants.SyncCurrencyRatesJobKey)
                .Build();

            //рабочий триггер для синхронизации
            var workingTrigger = TriggerBuilder.Create()
                .WithIdentity(Constants.SyncCurrencyRatesTriggerKey)
                .WithCronSchedule(Properties.Settings.Default.SyncRateTriggerCronExpression, csb =>
                    csb.WithMisfireHandlingInstructionDoNothing())
                .Build();

            //триггер при возникновении ошибки
            var failureTrigger = TriggerBuilder.Create()
                .WithIdentity(Constants.FailureTriggerKey)
                .StartAt(DateTimeOffset.Now.AddSeconds(Properties.Settings.Default.WaitIntervalInSeconds))
                .WithDailyTimeIntervalSchedule(dti => dti
                    .WithIntervalInSeconds(Properties.Settings.Default.WaitIntervalInSeconds)
                    .WithMisfireHandlingInstructionDoNothing())
                .Build();

            var jobFailureHandler = new JobFailureListener(workingTrigger, failureTrigger);

            scheduler.ListenerManager.AddJobListener(jobFailureHandler);
            scheduler.ScheduleJob(job, workingTrigger);
        }

        protected override void OnStart(string[] args)
        {
            log.Info("Служба запущена");
            RunScheduler();
        }

        protected override void OnPause()
        {
            scheduler?.PauseAll();
            log.Info("Работа службы приостановлена");
        }

        protected override void OnContinue()
        {
            scheduler?.ResumeAll();
            log.Info("Работа службы возобновлена");
        }

        protected override void OnStop()
        {
            scheduler?.Shutdown();
            log.Info("Служба остановлена");
        }
    }
}
