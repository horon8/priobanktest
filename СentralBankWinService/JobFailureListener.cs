﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace СentralBankWinService
{
    class JobFailureListener : IJobListener
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Name => Constants.JobFailureListenerName;

        /// <summary>
        /// Рабочий триггер
        /// </summary>
        public ITrigger WorkingTrigger { get; set; }

        /// <summary>
        /// Триггер работающий при возникновении ошибки
        /// </summary>
        public ITrigger FailureTrigger { get; set; }

        /// <summary>
        /// Listener перепланирует задачу с рабочего триггера на ошибочный и наобород
        /// </summary>
        /// <param name="workingTrigger">Рабочий триггер</param>
        /// <param name="failureTrigger">Триггер работающий при возникновении ошибки</param>
        public JobFailureListener(ITrigger workingTrigger, ITrigger failureTrigger)
        {
            WorkingTrigger = workingTrigger ?? throw new ArgumentNullException(nameof(workingTrigger));
            FailureTrigger = failureTrigger ?? throw new ArgumentNullException(nameof(failureTrigger));
        }

        public void JobToBeExecuted(IJobExecutionContext context) { }

        public void JobExecutionVetoed(IJobExecutionContext context) { }

        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            if (jobException == null)
            {
                //Восстанавливает рабочий триггер после успешной попытки
                if (context.Trigger.Key == Constants.FailureTriggerKey)
                    context.Scheduler.RescheduleJob(context.Trigger.Key, WorkingTrigger);

                return;
            }

            if (context.Trigger.Key != Constants.FailureTriggerKey)
            {
                context.Scheduler.RescheduleJob(context.Trigger.Key, FailureTrigger);
            }
        }
    }
}
