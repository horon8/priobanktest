﻿using DatabaseLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using СentralBankServices.CBServices;

namespace СentralBankServices
{
    public class SyncCurrencyRates
    {
        private DatabaseContext db;
        private DailyInfoSoap client;

        public SyncCurrencyRates(DailyInfoSoap client, DatabaseContext db)
        {
            this.db = db ?? throw new ArgumentNullException(nameof(db));
            this.client = client ?? throw new ArgumentNullException(nameof(client));
        }

        /// <summary>
        /// Синхронизация курса валют на дату
        /// </summary>
        /// <param name="dateRate">Дата синхронизации</param>
        public void SyncCurrencyRatesOnDate(DateTime dateRate)
        {
            var ratesCB = GetCurrencyRatesOnDate(dateRate);
            SaveCurrencyRatesToBase(ratesCB);
        }

        /// <summary>
        /// Получить курс валюты на дату
        /// </summary>
        /// <param name="dateCurs">Дата</param>
        /// <returns></returns>
        public IEnumerable<CurrencyRateOnDate> GetCurrencyRatesOnDate(DateTime dateCurs)
        {
            var dataSet = client.GetCursOnDate(dateCurs);
            var dateCB = GetCBDate(dataSet);

            if (dateCurs.Date == dateCB.Date)
            {
                throw new Exception($"Дата запроса {dateCurs:dd.MM.yyyy}, " +
                    $"дата ответа сервиса {dateCB:dd.MM.yyyy}");
            }

            return MapToList(dataSet);
        }

        /// <summary>
        /// Сохранить курсы валют в бд
        /// </summary>
        /// <param name="currencyRatesCB">Курсы валют</param>
        public void SaveCurrencyRatesToBase(IEnumerable<CurrencyRateOnDate> currencyRatesCB)
        {
            foreach (var currencyRateCB in currencyRatesCB)
            {
                var currencyRateDb = db.CurrencyRatesOnDate.SingleOrDefault(r => r.Date == currencyRateCB.Date && r.Code == currencyRateCB.Code);
                if (currencyRateDb == null)
                {
                    currencyRateDb = new CurrencyRateOnDate()
                    {
                        Date = currencyRateCB.Date,
                        Code = currencyRateCB.Code,
                        ChCode = currencyRateCB.ChCode
                    };

                    db.CurrencyRatesOnDate.Add(currencyRateDb);
                }

                currencyRateDb.Name = currencyRateCB.Name.Trim();
                currencyRateDb.Rate = currencyRateCB.Rate;
                currencyRateDb.Denomination = currencyRateCB.Denomination;

                db.SaveChanges();
            }
        }

        private List<CurrencyRateOnDate> MapToList(DataSet dataSet)
        {
            var date = GetCBDate(dataSet);
            return dataSet.Tables["ValuteCursOnDate"]
                .AsEnumerable()
                .Select(dataRow => new CurrencyRateOnDate
                {
                    Date = date,
                    Name = dataRow.Field<string>("Vname"),
                    Denomination = dataRow.Field<decimal>("Vnom"),
                    Rate = dataRow.Field<decimal>("Vcurs"),
                    ChCode = dataRow.Field<string>("VchCode"),
                    Code = dataRow.Field<int>("Vcode"),
                }).ToList();
        }

        private DateTime GetCBDate(DataSet dataSet)
        {
            return DateTime.ParseExact(dataSet.ExtendedProperties["OnDate"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
        }
    }
}
